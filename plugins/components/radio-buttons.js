import Vue from 'vue'
import RadioButtons from '@/components/elements/RadioButtons.vue'

Vue.component('m-radio-buttons', RadioButtons)