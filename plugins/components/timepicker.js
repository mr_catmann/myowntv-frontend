import Vue from 'vue'
import Timepicker from '@/components/elements/Timepicker.vue'

Vue.component('m-timepicker', Timepicker)