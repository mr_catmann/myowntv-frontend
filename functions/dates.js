import { distanceInWords, format } from 'date-fns';

const locales = {
  en: require('date-fns/locale/en'),
  ru: require('date-fns/locale/ru')
};

export function getLocales() {
  return locales;
}
export function getPublishDate(date, isTimestamp = true) {
    let now = new Date();
    let then = new Date(isTimestamp ? date * 1000 : date);
    let nowTs = Math.floor(now.getTime() / 1000);
    if (nowTs - date < 60 * 60 * 24 * 30) {
      let result = distanceInWords(
        now,
        then,
        {
          addSuffix: true,
          locale: locales[window.__locale__]
        }
      );
      return result;
    } else {
      let dateFormat = 'D MMMM YYYY';
      if (nowTs - date < 60 * 60 * 24 * 365) {
        dateFormat = 'D MMMM';
      }
      let result = format(then,
        dateFormat,
        {locale: locales[window.__locale__]}
      );
      return result;
    }
}

export function getFullDate(date, isTimestamp = true, seconds = true) {
  let then = new Date(isTimestamp ? date * 1000 : date);
  let dateFormat = "DD.MM.YYYY H:mm" + (seconds ? ":ss" : "");
  if (window.__locale__ === "ru") {
    dateFormat = "DD.MM.YYYY в H:mm" + (seconds ? ":ss" : "");
  }
  let result = format(then,
    dateFormat,
    {locale: locales[window.__locale__]}
  );
  return result;
}

export function getDate(ts) {
  let then = new Date(ts);
  let dateFormat = "DD MMMM";
  let result = format(then,
    dateFormat,
    {locale: locales[window.__locale__]}
  );
  return result;
}


export function getDateWithYear(ts) {
  let then = new Date(ts * 1000);
  let dateFormat = "DD.MM.YYYY";
  let result = format(then,
    dateFormat,
    {locale: locales[window.__locale__]}
  );
  return result;
}


export function getTime(ts) {
  let then = new Date(ts * 1000);
  let dateFormat = "H:mm:ss";
  let result = format(then,
    dateFormat,
  );
  return result;
}

