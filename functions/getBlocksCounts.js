let getBlocksCounts = (config) => {
  const windowWidth = window.innerWidth, windowHeight = window.innerHeight;
  let w,h=1;
  Object.keys(config.width).forEach(item=>{
    if (windowWidth >= item) {
      w = config.width[item];
    }
  });
  Object.keys(config.height).forEach(item=>{
    if (windowHeight >= item) {
      h = config.height[item];
    }
  });
  return {w,h};
};
export default getBlocksCounts;
