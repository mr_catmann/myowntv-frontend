import Vue from 'vue'
import Modal from '@/components/elements/Modal.vue'

Vue.component('m-modal', Modal)