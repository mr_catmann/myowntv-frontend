const path = require('path')
const webpack = require('webpack')
module.exports = {
  /*
  ** Headers of the page
  */
  mode: 'spa',
  head: {
    titleTemplate: (title) => {
      if (!title) {
        return 'Catcast';
      }
      return `${title} | Catcast`;
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'catcast.tv - live broadcasting and radio' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],

    //script: [
      //{ src: '//gstatic.com/cv/js/sender/v1/cast_sender.js', charset: 'utf-8'}
    //]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#107fd0' },
  loadingIndicator: 'assets/loading.html',
  /*
  ** Build configuration
  */
  plugins:[
	  { src: '~/plugins/i18n.js'},
	  { src: '~/plugins/components/autocomplete.js'},
	  { src: '~/plugins/components/button.js'},
	  { src: '~/plugins/components/checkbox.js'},
	  { src: '~/plugins/components/colorpicker.js'},
	  { src: '~/plugins/components/datetime.js'},
	  { src: '~/plugins/components/filepicker.js'},
	  { src: '~/plugins/components/editor.js'},
	  { src: '~/plugins/components/error-page.js'},
    { src: '~/plugins/components/infinite-scroll.js'},
	  { src: '~/plugins/components/input.js'},
	  { src: '~/plugins/components/form.js'},
	  { src: '~/plugins/components/nothing-found.js'},
	  { src: '~/plugins/components/pager.js'},
    { src: '~/plugins/components/popup-menu.js'},
	  { src: '~/plugins/components/preloader.js'},
    { src: '~/plugins/components/slider.js'},
	  { src: '~/plugins/components/modal.js'},
	  { src: '~/plugins/components/select.js'},
	  { src: '~/plugins/components/radio-buttons.js'},
	  { src: '~/plugins/components/timepicker.js'},
	  { src: '~/plugins/components/response.js'},
	  { src: '~/plugins/components/tabs.js'},
	  { src: '~/plugins/components/tags-input.js'},
    { src: '~/plugins/dragula.js'},
    { src: '~/plugins/vue-echo.js'},
	  { src: '~/plugins/ripple.js'},
	  { src: '~/plugins/quill.js', ssr: false },
	  { src: '~/plugins/nuxt-client-init.js', ssr: false },
	  '~/plugins/axios',
  ],
  modules: [
	  '@nuxtjs/axios',
    '@nuxtjs/pwa',
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '54471244',
        webvisor: false,
        // clickmap:true,
        // useCDN:false,
        // trackLinks:true,
        // accurateTrackBounce:true,
      }
    ],
  ],
  css:[
	  'quill/dist/quill.snow.css',
    'quill/dist/quill.bubble.css',
    'quill/dist/quill.core.css',
	  './assets/styles/global.scss',
  ],
  axios: {
      baseURL: 'https://api.catcast.tv/api',
      headers: {
        'Content-Type':'application/json',
      },
      credentials: false,
      requestInterceptor: (config, {store}) => {
          config.headers.common['Authorization'] = store.state.token;
          config.headers.common['X-Refresh-Token'] = store.state.refresh_token;
          return config
      }
  },
  build: {
    transpile: [
        'mediasoup-client'
    ],
    extend (config, { isDev }) {
      if (isDev && process.client) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
        config.module.rules.push({
          test: require.resolve('video.js'),
          use: [{
            loader: 'expose-loader',
            options: 'videojs'
          }]
        })
        config.module.rules.push({
          test: require.resolve('flv.js'),
          use: [{
            loader: 'expose-loader',
            options: 'flvjs'
          }]
        })
      }
    }
  },
}
