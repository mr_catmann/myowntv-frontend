const tickets = {
  namespaced: true,
  state: {
    tickets: {
      count: 0,
      is_loading: false,
      important_messages_window_visible: false,
    }
  },
  mutations: {
    set(state, {unread_count, has_important_messages}) {
      state.tickets.unread_count = unread_count;
      state.tickets.important_messages_window_visible = has_important_messages;
    }
  },
  actions: {
    async get({commit}) {
      let tickets = (await this.$axios.post('tickets/unread')).data;
      if (tickets.status) {
        commit('set', tickets.data);
      }
    }
  },
  getters: {

  }
};
export default tickets;
