let loadLocaleMessages = function (cb) {
    return api.post('/index.php').then((res) => {
      return res.data
    }).then((json) => {
      if (Object.keys(json).length === 0) {
        return Promise.reject(new Error('Ошибка загрузки языковых данных'))
      } else {
        return Promise.resolve(json)
      }
    }).then((message) => {
      cb(null, message)
    }).catch((error) => {
      cb(error)
    })
}
export default loadLocaleMessages;