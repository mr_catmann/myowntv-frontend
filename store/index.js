import Vue from 'vue'
import Vuex from 'vuex'
import jwtDecode from 'jwt-decode'
import cookies from '~/functions/cookies.js';
import mail from '@/store/mail.js';
import modals from '@/store/modals.js';
import notifications from '@/store/notifications.js';
import friends from '@/store/friends.js';
import tickets from '@/store/tickets.js';
import players from '@/store/players.js';
Vue.use(Vuex);
const store = () => new Vuex.Store({
  modules:{
    mail,
    modals,
    site_notifications: notifications,
    friends,
    tickets,
    players
  },
  state: {
    apiURL: "https://api.catcast.tv/api/",
		alerts: [],
    notifications: [],
		token: null,
		refreshToken: false,
		userData: {
			loggedIn: false,
			id: -1,
			username: '',
			avatar: '',
		},
		drawer: (typeof localStorage !== 'undefined' && localStorage.drawer === 'true') || false,
		locales: ['ru', 'en'],
		locale: typeof localStorage.locale !== 'undefined' ? localStorage.locale : 'ru',
		tokenParsed: false,
		player: {
			visible: false,
			currentTrack: null,
      element: null,
      needLoad: false,
		},
    currentTheme: localStorage.currentTheme || "flat",
    isInitialised: false,
	},
  getters:{
    loggedIn(state) {
      return state.userData ? state.userData.loggedIn : false;
    }
  },
	actions:{
		async setUserData({ commit, state, store, context}, data) {
			this.$axios.defaults.headers.common['X-Timezone-Offset'] = data.timezoneOffset || 0;
			if (data.token && data.token.length > 0) {
				  try {
             let payload = jwtDecode(data.token);
             commit('SET_TOKENS', {token: data.token, doNotSetCookie: true});
					   let res = await this.$axios.post('/auth/getmydata');
					   if (res.data.status) {
					      commit('SET_USER_DATA',res.data.data);

             } else {
					     //commit('LOGOUT_USER');
             }
				  } catch (e) {
					  console.log(e);
			  	}
			}
			if (data.refreshToken && data.refreshToken.length > 0) {
			    commit('SET_TOKENS', {
			      refreshToken: data.refreshToken,
            doNotSetCookie: true
			    })
      }
		},
		async nuxtServerInit ({ commit, dispatch }, context) {
			let req = context.req;
			if (req && req.headers && req.headers.cookie) {
			  let cookie = req.headers.cookie.split(';');
				let token = cookie.find(c => c.trim().startsWith('token='));
				if (token){
					token = token.split('=')[1];
				}
				let refreshToken = cookie.find(c => c.trim().startsWith('refresh='));
				if (refreshToken) {
					refreshToken=refreshToken.split('=')[1];
				}
				let timezoneOffset = cookie.find(c => c.trim().startsWith('timezone_offset='));
				if (timezoneOffset) {
					timezoneOffset = timezoneOffset.split('=')[1];
				}
				await dispatch('setUserData', {
				  token: token,
          refreshToken: refreshToken,
          timezoneOffset: timezoneOffset
				});
				return true;
			} else{
				return true;
			}
		},
		async nuxtClientInit({dispatch, commit, state, }) {
      if (!state.isInitialised) {
        let token = cookies.get('token');
        let refreshToken = cookies.get('refresh_token');
        let timezoneOffset = new Date().getTimezoneOffset();
        await dispatch('setUserData', {
          token: token,
          refreshToken: refreshToken,
          timezoneOffset: timezoneOffset
        });
        window.__locale__ = "ru";
        if (state.userData.loggedIn) {
          await dispatch('site_notifications/get');
          await dispatch('friends/getRequests');
          await dispatch('tickets/get');
        }
      }
    }
	},
	mutations: {
    SET_CURRENT_THEME(state, theme) {
      localStorage.currentTheme = theme;
      state.currentTheme = theme;
    },
    HIDE_PLAYER_WITHOUT_UNLOAD(state) {
      Vue.set(state,'player',{
        visible: false,
      })
    },
    HIDE_PLAYER(state) {
      Vue.set(state,'player',{
        visible: false,
        track: null
      })
    },
    SET_PLAYER_TRACK_WITHOUT_LOAD(state, {track}) {
      Vue.set(state,'player',{
        visible: true,
        track,
        needLoad: false
      })
    },
    SET_PLAYER_TRACK_ELEMENT(state, {track, element}) {
      document.body.appendChild(element);
      Vue.set(state,'player',{
        visible: true,
        track,
        element
      })


    },
		SET_PLAYER_TRACK(state,track) {
			Vue.set(state,'player',{
				visible: true,
				track: track,
        needLoad: true,
			})
		},
		LOGOUT_USER(state){
      state.userData = {};
			state.userData.loggedIn=false;
			state.token = null;
			state.refreshToken = null;
			cookies.set('token',null,-1);
			cookies.set('refresh_token',null,-1);
		},
		TOKEN_PARSED(state) {
			state.tokenParsed=true;
		},
		SET_USER_DATA (state,data) {
		  state.isInitialised = true;
			state.userData = data;
			state.userData.loggedIn = true;
		},
    SET_LOCALE(state, locale) {
      state.locale = locale;
    },
		SET_TOKENS(state,data) {
			if (data.token) {
				if (!data.doNotSetCookie) {
						cookies.set('token',data.token,60*24*7);
				}
				this.$axios.setHeader('Authorization',"Bearer "+data.token);
				state.token=data.token;
			}
			if (data.refreshToken) {
				state.refreshToken=data.refreshToken;
				if (!data.doNotSetCookie) {
						cookies.set('refresh_token',data.refreshToken,60*24*30);
				}
			}
		},
		SET_DRAWER (state) {
			localStorage.drawer = !state.drawer;
			state.drawer = !state.drawer;
		},
    CLOSE_NOTIFICATION(state, data) {
      state.notifications = state.notifications.filter(notification => {
        return notification.id !== data.id;
      });
    },
    NEW_NOTIFICATION(state, data) {
      let id = Math.floor(Math.random()*10000);
      console.log(data);
      let notification = {
        title: data.title,
        text: data.text,
        notification_link: data.notification_link,
        picture: data.picture,
        translate: data.translate,
        id,
      };
      if (data.need_increment) {
        state.site_notifications.site_notifications.need_to_get = true;
        state.site_notifications.site_notifications.unread_count++;
      }
      state.notifications.push(notification);
      setTimeout(() => {
        state.notifications = state.notifications.filter(notification => {
          return notification.id !== id;
        });
      }, 15000);
    },
		NEW_ALERT(state,data) {
			let id = Math.floor(Math.random()*10000);
			state.alerts.push({
				no_translate: data.no_translate,
				text: data.text,
				status: data.status,
				id
			});
			setTimeout(()=>{
				let alerts = state.alerts.filter(notification=>{
					return notification.id !== id;
				});
				state.alerts = alerts;
			}, 5000);
		}
	}
});

export default store
