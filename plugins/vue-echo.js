import Vue from 'vue';
import VueEcho from 'vue-echo';

window.io = require('socket.io-client');
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
Vue.use(VueEcho, {
  broadcaster: 'socket.io',
  host: 'https://ws.catcast.tv',
  auth: {
    headers: {
      'Authorization': 'Bearer ' + getCookie('token'),
      'X-Chat-Color': localStorage.chat_color || '#fff',
      'X-Guest-Username': localStorage.guest_username || 'Guest',
    },
  },
});
