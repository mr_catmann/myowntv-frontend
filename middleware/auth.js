export default async function ({ route, store, redirect }) {
  await store.dispatch('nuxtClientInit');
  if (!store.state.userData.loggedIn) {
    return redirect('/login?return='+route.fullPath)
  }
}
