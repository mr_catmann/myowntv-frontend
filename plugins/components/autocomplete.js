import Vue from 'vue'
import Autocomplete from '@/components/elements/Autocomplete.vue'

Vue.component('m-autocomplete', Autocomplete)