import Vue from 'vue'
import PopupMenu from '@/components/elements/popup-menu/PopupMenu.vue'
import PopupMenuHeader from '@/components/elements/popup-menu/PopupMenuHeader.vue'
import PopupMenuItem from '@/components/elements/popup-menu/PopupMenuItem.vue'

Vue.component('m-popup-menu', PopupMenu)
Vue.component('m-popup-menu-header', PopupMenuHeader)
Vue.component('m-popup-menu-item', PopupMenuItem)
