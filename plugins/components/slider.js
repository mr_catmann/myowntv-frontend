import Vue from 'vue'
import Slider from '@/components/elements/Slider.vue'

Vue.component('m-slider', Slider)
