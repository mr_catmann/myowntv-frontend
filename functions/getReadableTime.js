const getReadableTime = (time) => {
  time = Math.ceil(time);
  let hoursString = '00';
  let minutesString = '00';
  let secondsString = '00';
  let hours = 0;
  let minutes = 0;
  hours = Math.floor( time / 3600);
  time = time % 3600;
  minutes = Math.floor( time / 60);
  time = time % 60;
  hoursString = hours.toString();
  minutesString = (minutes >= 10) ? minutes.toString() : '0' + minutes.toString();
  secondsString = (time >= 10) ? time.toString() : '0' + time.toString();
  return (((hours > 0) ? hoursString + ':' : '') + minutesString + ':' + secondsString);
};
export default getReadableTime;
