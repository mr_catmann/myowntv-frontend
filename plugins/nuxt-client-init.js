export default async (ctx) => {
	let langs = (await ctx.$axios.get('https://api.catcast.tv/public/languages.json?t='+(new Date().getTime()))).data;
	if (langs instanceof String) {
    langs = JSON.parse(langs);
	}
	Object.keys(langs).forEach(lang=>{
		ctx.app.i18n.setLocaleMessage(lang, langs[lang]);
	});
  await ctx.store.dispatch('nuxtClientInit', ctx)
}
