const modals = {
  namespaced: true,
  state: {
    attachments: {
      visible: false,
      index: 0,
      data: []
    }
  },
  mutations: {
    SHOW_ATTACHMENTS_MODAL(state, {attachments, index, visible}) {
      if (visible === undefined) {
        state.attachments.visible = true;
      } else {
        state.attachments.visible = visible;
      }
      if (attachments) {
        state.attachments.data = attachments;
      }
      if (index !== undefined) {
        state.attachments.index = index;
      }
    }
  },
  actions: {

  },
  getters: {

  }
};
export default modals;
