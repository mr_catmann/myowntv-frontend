import Vue from 'vue'
import Colorpicker from '@/components/elements/Colorpicker.vue'

Vue.component('m-colorpicker', Colorpicker)