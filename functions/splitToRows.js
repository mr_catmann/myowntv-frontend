const splitToRows=((list,count)=>{
	let newList=[];
	let i,j=0;
	let listItem=[];
	for (i=0,j=list.length; i<j; i+=count) {
		listItem = list.slice(i,i+count);
		newList.push(listItem); 
	}
	return newList;
})

export default splitToRows;