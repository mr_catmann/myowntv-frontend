import Vue from 'vue'
import TagsInput from '@/components/elements/TagsInput.vue'

Vue.component('m-tags-input', TagsInput)