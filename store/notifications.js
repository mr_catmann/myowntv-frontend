const notifications = {
  namespaced: true,
  state: {
    site_notifications: {
      list: [],
      unread_count: 0,
      is_loading: false,
      need_to_get: false,
    }
  },
  mutations: {
    set(state, {list, unread_count}) {
      state.site_notifications.list = list;
      state.site_notifications.unread_count = unread_count;
    }
  },
  actions: {
    read({state}) {
      if (state.site_notifications.unread_count > 0 && !state.site_notifications.is_loading) {
        state.site_notifications.is_loading = true;
        this.$axios.post('notifications/read').then(res => {
          state.site_notifications.is_loading = false;
          if (res.data.status) {
            state.site_notifications.unread_count = 0;
          }
        });
      }
    },
    async get({state, commit}) {
      state.site_notifications.need_to_get = false;
      state.site_notifications.is_loading = true;
      let notifications = (await this.$axios.get('notifications?count=5')).data;
      if (notifications.status) {
        commit('set', {list: notifications.data.list.data, unread_count: notifications.data.unread_count});
      }
      state.site_notifications.is_loading = false;
    }
  },
  getters: {
    isLoading(state) {
      return state.site_notifications.is_loading;
    },
    needToGet(state) {
      return state.site_notifications.need_to_get;
    },
    getList(state) {
      return state.site_notifications.list;
    },
    getUnreadCount(state) {
      return state.site_notifications.unread_count;
    }
  }
};
export default notifications;
