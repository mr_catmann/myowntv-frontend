import Vue from 'vue'
import NothingFound from '@/components/elements/NothingFound.vue'

Vue.component('m-nothing-found', NothingFound)