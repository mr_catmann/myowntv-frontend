import Vue from 'vue'
import Filepicker from '@/components/elements/Filepicker.vue'

Vue.component('m-filepicker', Filepicker)