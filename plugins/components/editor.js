import Vue from 'vue'
import Editor from '@/components/elements/Editor.vue'

Vue.component('m-editor', Editor)