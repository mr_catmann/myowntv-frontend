const desk = {
  namespaced: true,
  state: {
    desk: {
      visible: false,
      list: [],
    }
  },
  mutations: {
    set(state, {notifications, unread_count}) {
      state.site_notifications.list = notifications;
      state.site_notifications.unread_count = unread_count;
    }
  },
  actions: {
    read({state}) {
      if (state.site_notifications.unread_count > 0 && !state.site_notifications.is_loading) {
        state.site_notifications.is_loading = true;
        this.$axios.post('notifications/read').then(res => {
          state.site_notifications.is_loading = false;
          if (res.data.status) {
            state.site_notifications.unread_count = 0;
          }
        });
      }
    },
    async get({commit}) {
      let notifications = (await this.$axios.post('notifications')).data;
      if (notifications.status) {
        commit('set', notifications.data);
      }
    }
  },
  getters: {

  }
};
export default desk;
