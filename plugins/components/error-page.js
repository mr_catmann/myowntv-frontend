import Vue from 'vue'
import ErrorPage from '@/components/elements/ErrorPage.vue'

Vue.component('m-error-page', ErrorPage)