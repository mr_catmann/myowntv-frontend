import Vue from 'vue'
import InfiniteScroll from '@/components/elements/InfiniteScroll.vue'
Vue.component('m-infinite-scroll', InfiniteScroll)
