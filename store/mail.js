import Vue from 'vue';
const mail = {
  namespaced: true,
  state: {
    dialogs: {},
    messages: {},
    pagesCounts: {},
    dialogsUsers: {},
    dialogsOnPage: 20,
    dialogsCount: 0,
    dialogsLoading: false,
  },
  mutations: {
    setCount(state, total) {
      state.dialogsCount = total;
    },
    setLoading(state, loading) {
      state.dialogsLoading = loading;
    },
    setMessages(state, {user_id, messages, page, pagesCount, user}) {
      let messagesData;
      if (!state.messages[user_id]) {
        messagesData = {};
      } else {
        messagesData = state.messages[user_id];
      }
      messagesData[page] = messages;
      Vue.set(state.messages, user_id, messagesData);
      if (pagesCount) {
        state.pagesCounts[user_id] = pagesCount;
      }
      if (user) {
        state.dialogsUsers[user_id] = user;
      }
    },
    setDialogs(state, {dialogs, page}) {
      Vue.set(state.dialogs, page, dialogs);
    }
  },
  actions: {
    readDialog(ctx, {user_id, is_out}) {
      let dialogs = ctx.state.dialogs;
      user_id = parseInt(user_id);
      Object.keys(dialogs).forEach(page=>{
        dialogs[page] = dialogs[page].map(dialog=>{
          if ((!dialog.is_out || is_out) && user_id === dialog.user_id) {
            dialog.is_read = true;
          }
          return dialog;
        })
      });
      if (ctx.state.messages[user_id]) {
        let messages = ctx.state.messages[user_id][1];
        messages = messages.map(message=>{
          if (!message.is_out || is_out) {
            message.is_read = true;
          }
        })
      }
    },
    editMessage(ctx, {user_id, message}) {
      user_id = parseInt(user_id);
      if (ctx.state.messages[user_id]) {
        let messages = ctx.state.messages[user_id];
        Object.keys(messages).forEach(page=> {
          messages[page].forEach((messageItem, index)=> {
            if (messageItem.id === message.id) {
              messages[page][index] = message;
            }
            ctx.commit('setMessages', {
              user_id,
              messages: messages[page],
              page: page
            });
          })
        })
      }
      let copied = JSON.parse(JSON.stringify(message));
      let dialogs = ctx.state.dialogs;
      Object.keys(dialogs).forEach(page=>{
          dialogs[page].forEach((dialog, index)=>{
            if (dialog.id === copied.id) {
              copied.user = dialog.user;
              dialogs[page][index] = copied;
              ctx.commit('setDialogs', {dialogs: dialogs[page], page});
            }
          })
      });
    },
    newMessage(ctx, {user_id, message}) {
      user_id = parseInt(user_id);
      if (ctx.state.messages[user_id]) {
        let messages = ctx.state.messages[user_id][1];
        messages.unshift(message);
        ctx.commit('setMessages', {
          user_id,
          messages,
          page: 1
        });
      }
      let dialogs = ctx.state.dialogs;
      Object.keys(dialogs).forEach(page=>{
        if (dialogs[page].filter(dialog=>{
            return ((dialog.is_out && dialog.to_id === user_id) || (!dialog.is_out && dialog.from_id === user_id));
        }).length !== 0) {
          let dialogsPageList = dialogs[page].filter(dialog=> {
            return (!((dialog.is_out && dialog.to_id === user_id) || (!dialog.is_out && dialog.from_id === user_id)));
          });
          ctx.commit('setDialogs', {dialogs: dialogsPageList, page});
        }
      });
      let data = JSON.parse(JSON.stringify(message));
      if (data.is_out) {
        data.user = data.to;
      } else {
        data.user = data.from;
      }
      let firstPage = dialogs[1];
      firstPage.unshift(data);
      ctx.commit('setDialogs', {dialogs: firstPage, page: 1});
    },
    loadMessages(ctx, {user_id, page}) {
      return new Promise((resolve,reject)=> {
        if (!ctx.state.messages[user_id]) {
          // ctx.commit('setLoading', true);
        }
        this.$axios.get('mail/dialogs/'+user_id+'?page=' + page).then(res => {
          let messages = res.data.list;
          ctx.commit('setMessages', {user_id, messages: messages.data, page, pagesCount: messages.last_page, user: res.data.user});
          resolve();
        })
      })
    },
    loadDialogs(ctx, page) {
      return new Promise((resolve,reject)=> {
        if (Object.keys(ctx.state.dialogs).length === 0) {
          ctx.commit('setLoading', true);
        }
        this.$axios.get('mail?page=' + page).then(res => {
          let dialogs = res.data.list;
          ctx.commit('setCount', dialogs.total);
          ctx.commit('setDialogs', {dialogs: dialogs.data, page});
          ctx.commit('setLoading', false);
          resolve();
        })
      })
    }
  },
  getters: {

  }
};
export default mail;
