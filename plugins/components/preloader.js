import Vue from 'vue'
import Preloader from '@/components/elements/Preloader.vue'

Vue.component('m-preloader', Preloader)