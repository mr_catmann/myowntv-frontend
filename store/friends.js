const friends = {
  namespaced: true,
  state: {
    friends_requests: {
      list: [],
      count: 0,
      is_loading: false,
    }
  },
  mutations: {
    set(state, {count, list}) {
      state.friends_requests.count = count;
      state.friends_requests.list = list;
    }
  },
  actions: {
    async getRequests({commit}) {
      let friends_requests = (await this.$axios.post('auth/friendsrequests')).data;
      if (friends_requests.status) {
        commit('set', friends_requests.data);
      }
    }
  },
  getters: {
    getList(state) {
      return state.friends_requests.list;
    },
    getCount(state) {
      return state.friends_requests.count;
    }
  }
};
export default friends;
