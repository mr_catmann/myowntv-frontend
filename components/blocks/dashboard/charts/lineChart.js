import { Line } from 'vue-chartjs'

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}



export default {
  extends: Line,
  methods: {
    render() {
      console.log(this.data);
      this.renderChart({
        labels: this.labels,
        datasets: this.data.map((item, index) => {
          return {
            label: this.titles ? this.titles[index] : "",
            borderColor: this.colors ? this.colors[index] : getComputedStyle(document.getElementById('app')).getPropertyValue('--active-color'), //,
            data: item
          }
        })
      }, {
        maintainAspectRatio: false,
        legend: {
          display: true,
          position: 'bottom',
          labels: {

          }
        },
        tooltips: {
          callbacks: {
            label: (tooltipItem, data) =>  {
              if (this.tooltips) {
                return this.tooltips[tooltipItem.index]+" - "+tooltipItem.yLabel;
              }
              return tooltipItem.yLabel+" ";
            }
          }
        }
      })
    }
  },
  watch: {
    data() {
      this.render();
    }
  },
  mounted () {
    this.render();
  },
  props: {
    titles: {
      type: Array,
      required: false
    },
    colors: {
      type: Array,
      required: false
    },
    tooltips: {
      type: Array,
      required: false,
    },
    labels: {
      type: Array,
      required: true,
    },
    title: {
      type: String,
      required: false,
    },
    data: {
      type: Array,
      required: true,
    }
  }
}
