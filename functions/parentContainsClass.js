export default function parentContainsClass(child, elementClass) {
  let node = child.parentNode;
  while (node != null) {
    if (node.classList && node.classList.contains(elementClass)) {
      return true;
    }
    node = node.parentNode;
  }
  return false;
}
