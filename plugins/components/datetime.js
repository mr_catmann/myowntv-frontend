import Vue from 'vue'
import DateTimeInput from '@/components/elements/DateTimeInput.vue'

Vue.component('m-datetime', DateTimeInput);
